package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
@EnableWebSecurity
public class ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

  private static final String CLIENT_ID = "resourceServer";
  private static final String CLIENT_SECRET = "123";
  private static final String TOKEN_INTROSPECTION_ENDPOINT =
      "http://localhost:8080/spring-auth-server/oauth/check_token";

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    resources //
        .stateless(true) // only token-based authentication is allowed
        .tokenServices(tokenServices());
  }

  @Bean
  public ResourceServerTokenServices tokenServices() {
    RemoteTokenServices tokenServices = new RemoteTokenServices();
    tokenServices.setCheckTokenEndpointUrl(TOKEN_INTROSPECTION_ENDPOINT);
    tokenServices.setClientId(CLIENT_ID);
    tokenServices.setClientSecret(CLIENT_SECRET);

    return tokenServices;
  }
}
