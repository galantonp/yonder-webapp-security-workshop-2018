package com.example.demo.repository;

import java.util.List;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;

public interface OrderRepository {

  Order create(String username, List<OrderItem> items);
  
  List<Order> findAll(String username);
  
  Order find(long orderId);
  
}
