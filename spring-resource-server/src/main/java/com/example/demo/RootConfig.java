package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.memory.InMemoryOrderRepository;
import com.example.demo.repository.memory.InMemoryProductRepository;

@Configuration
@ComponentScan
@EnableWebMvc
public class RootConfig {

  @Bean
  public OrderRepository orderRepository() {
    return new InMemoryOrderRepository();
  }

  @Bean
  public ProductRepository productRepository() {
    return new InMemoryProductRepository();
  }

}
