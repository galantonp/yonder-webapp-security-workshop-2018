package com.example.demo.model;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import org.junit.Test;

public class OrderItemTest {

  @Test
  public void testTotalPrice() {
    BigDecimal totalPrice = new OrderItem(new Product(1, "Orange", BigDecimal.valueOf(3)), 14).getTotalPrice();
    assertEquals(BigDecimal.valueOf(42), totalPrice);
  }
}
