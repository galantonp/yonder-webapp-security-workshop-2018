package com.example.demo;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Servlet API 3.1 initializer which registers the Spring Security filter.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
