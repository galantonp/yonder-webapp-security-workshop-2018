package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RevocationController {

  @Autowired
  private ConsumerTokenServices tokenServices;
  
  @PostMapping("/oauth/revoke_token")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void revokeToken(@RequestParam("access_token") String accessToken) {
    // TODO should check that access_token was issued to the calling client 
    tokenServices.revokeToken(accessToken);
  }
}
