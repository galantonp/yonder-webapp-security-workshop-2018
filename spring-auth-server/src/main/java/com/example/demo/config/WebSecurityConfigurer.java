package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    // configure users
    auth.inMemoryAuthentication() //
        .withUser("petru").password("{noop}123").roles("customer") //
        .and() //
        .withUser("paul").password("{noop}123").roles("employee");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http //
        .csrf().disable() // for ease of demonstration from SoapUI only !!
        .sessionManagement().sessionFixation().none() // 
        .and().formLogin(); // users log in using a username and password form
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.debug(false); // switch to true to log debugging information on each request
  }

  @Override
  @Bean("authenticationManager")
  public AuthenticationManager authenticationManagerBean() throws Exception {
    // expose the authentication manager as a bean
    return super.authenticationManagerBean();
  }

  @Override
  @Bean("userDetailsService")
  public UserDetailsService userDetailsServiceBean() throws Exception {
    // expose the user details service as a bean
    return super.userDetailsServiceBean();
  }
}
