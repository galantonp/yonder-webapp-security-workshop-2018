package com.example.demo.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

  private long id;
  private String username;
  private Date orderDate;
  private Date deliveryDate;
  private List<OrderItem> items = new ArrayList<>();

  public Order() {
  }
  
  public Order(long id, String username, Date orderDate) {
    this.id = id;
    this.username = username;
    this.orderDate = orderDate;
  }

  public long getId() {
    return id;
  }
  
  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }

  public Date getOrderDate() {
    return orderDate;
  }
  
  public void setOrderDate(Date orderDate) {
    this.orderDate = orderDate;
  }

  public Date getDeliveryDate() {
    return deliveryDate;
  }
  
  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public List<OrderItem> getItems() {
    return items;
  }

  public void setItems(List<OrderItem> items) {
    this.items = items;
  }
  
  public BigDecimal getTotalPrice() {
    return items.stream() //
        .map(OrderItem::getTotalPrice) //
        .reduce((x, y) -> x.add(y)) //
        .orElse(BigDecimal.ZERO);
  }

  

}
