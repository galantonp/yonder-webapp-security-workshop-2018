package com.example.demo.controller;

import static java.util.stream.Collectors.toMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.model.Product;

@Controller
public class UiController {

  @Autowired
  private OAuth2RestTemplate restTemplate;

  @GetMapping("/")
  public String home(Model model, Authentication authentication) {
    model.addAttribute("name", authentication.getName());

    return "home";
  }

  @GetMapping("/order")
  public String order(Model model) {

    model.addAttribute("products", getProducts());

    return "order";
  }

  @PostMapping("/order")
  public String processOrder(@RequestParam Map<String, String> items, Model model) {
    Map<Long, Product> products = getProducts().stream().collect(toMap(Product::getId, x -> x));

    List<OrderItem> orderItems = new ArrayList<>();
    try {
      for (Entry<String, String> entry : items.entrySet()) {
        long productId = Long.valueOf(entry.getKey());
        int amount = Integer.valueOf(entry.getValue());
        Product product = products.get(productId);
        if (product != null) {
          orderItems.add(new OrderItem(product, amount));
        }
      }
    } catch (NumberFormatException ex) {
      return "order";
    }

    Order order = restTemplate.postForObject("http://localhost:8080/spring-resource-server/orders",
        orderItems, Order.class);
    model.addAttribute("order", order);

    return "order_details";
  }

  @GetMapping("/logged_out")
  public String loggedOut() {
    return "logged_out";
  }

  private List<Product> getProducts() {
    ResponseEntity<List<Product>> response =
        restTemplate.exchange("http://localhost:8080/spring-resource-server/products",
            HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});

    return response.getBody();
  }
}

