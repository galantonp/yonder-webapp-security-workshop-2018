package com.example.demo.oauth;

import static com.example.demo.config.OAuthClientConfigurer.TOKEN_REVOCATION_ENDPOINT;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class RevokeTokenLogoutHandler implements LogoutHandler {

  private OAuth2RestTemplate restTemplate;
  
  public RevokeTokenLogoutHandler(OAuth2RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public void logout(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) {
      
      OAuth2AccessToken token = restTemplate.getOAuth2ClientContext().getAccessToken();
      if (token.isExpired()) {
        return;
      }
      
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
      
      MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
      body.add("access_token", token.getValue());
      
      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(body, headers);
      restTemplate.postForObject(TOKEN_REVOCATION_ENDPOINT, entity, String.class);
  }

}
